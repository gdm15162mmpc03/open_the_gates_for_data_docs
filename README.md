#Locationary

**Locationary** is an application made for moviemakers to help them get a view on the location where they want to go filming using Google maps and several datasets from The World Databank. It was made as school assignment for **Arteveldehogeschool - Grafische and digitale media**. This application was made in a learning environment and has no commercial purpose.

####Google maps

**Website:** https://www.google.be/maps

####The World Databank

**Website:** http://data.worldbank.org/  
######Datasets  
**Forest area:** http://data.worldbank.org/indicator/AG.LND.FRST.ZS  
**Diesel:** http://data.worldbank.org/indicator/EP.PMP.DESL.CD  
**Gasoline:** http://data.worldbank.org/indicator/EP.PMP.SGAS.CD  
**Merchandise Trade:** http://data.worldbank.org/indicator/TG.VAL.TOTL.GD.ZS  
**Water sources:** http://data.worldbank.org/indicator/SH.H2O.SAFE.RU.ZS  
**Internetconnections:** http://data.worldbank.org/indicator/IT.NET.SECR.P6  
**Agricultural land:** http://data.worldbank.org/indicator/AG.LND.AGRI.ZS  
**Mobile Cellular Subscriptions:** http://data.worldbank.org/indicator/AG.LND.AGRI.ZS  





####Arteveldehogeschool - Grafische en Digitale Media

**Website:** http://www.arteveldehogeschool.be  
	      http://www.arteveldehogeschool.be/ects  
	      http://www.arteveldehogeschool.be/bachelor-de-grafische-en-digitale-media  
**Twitter:** http://twitter.com/bachelorGDM  
**Facebook:** https://www.facebook.com/BachelorGrafischeEnDigitaleMedia?fref=ts  
	  
####Autors

Data+Design+HTML+CSS+PHP+jQuery: **Tijtgat Ellen**  
**Facebook:** https://www.facebook.com/ellen.tijtgat  
**Location:** Deinze, Belgium  

Data+Design+HTML+CSS+PHP+jQuery: **Notaert Mathias**  
**Facebook:** https://www.facebook.com/mathias.notaert  
**Location:** Gooik, Belgium  

Data+Design+HTML+CSS+PHP+jQuery: **De Lange Matthias**  
**Facebook:** https://www.facebook.com/matthias.delange.5  
**Location:** Deinze, Belgium  

####Teachers

######Philippe De Pauw - Waterschoot 


**Twitter:** http://twitter.com/drdynscript  
**Github:** http://github.com/drdynscript  
**Bitbucket:** http://bitbucket.org/drdynscript  
**Linkedin:** https://be.linkedin.com/in/philippe-de-pauw-5a5a336  
**Email:** philippe.depauw@arteveldehs.be  

#####Jonas Pottie

**Twitter:** http://twitter.com/jonaspottie  
**Linkedin:** https://www.linkedin.com/in/jonaspottie  
**Email:** jonas.pottie@arteveldehs.be  